import axios from "axios";
import {E_NAME_CARD_API_BASE_URL} from "../helper/constant";
import authHeader from "./Auth-header";

class ChannelService{
    getChannels(){
        return axios.get(E_NAME_CARD_API_BASE_URL + "/channel",{ headers: authHeader() })
            .catch(function (error) {
            })
    }
}

export default new ChannelService();