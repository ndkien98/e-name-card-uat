import React from 'react';
import ENameCardService from "../../service/ENameCardService";
import avatar from '../../assets/avatar.png'
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import {checkRole, getCookie} from "../../service/Auth-header";
import {validFullName, validMail, validPhone, validProfileFacebook} from "../../service/Validate";
import 'antd/dist/antd.css';
import "../../App.css";
import { Select } from 'antd';
import ChannelService from "../../service/ChannelService";
import {roleAdmin, roleAgency} from "../../helper/constant";
const { Option } = Select;

class ENameCardCreate extends React.Component{

    constructor(props) {
        super(props);
        checkRole();
        this.init();
        this.state = {
            avatar: 'avatar.png',
            email: '',
            facebookLink: '',
            fullName: '',
            phone: '',
            positions: '',
            file: '',
            codeAgent:'',
            channelId:1,
            channels: [],
            disableChannel: false
        }

        ChannelService.getChannels().then((res) => {
            let role = getCookie("ROLE_NAME");
            if (role === roleAdmin){
                this.setState({channels: res.data})
            }else {
                let channelId = Number(getCookie("CHANNEL_ID"));
                let channelResponse = [];
                channelResponse.push(res.data.filter((data) => data.id === channelId));
                this.setState({channels: channelResponse});
                this.setState({disableChannel: true})
                console.log(this.state)
            }
        })
    }

    handleChange = (e) => {
        let {name, value} = e.target
        if (name.toString() === 'fullName'){
            console.log(value)
            value = value.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
            value = value.replace(/\s\s+/g, ' ');
        }
        this.setState({[name]: value})
    }

    uploadImage = (e) => {
        const file = e.target.files[0];
        const preview = document.getElementById('viewAvatarP');
        const render = new FileReader();
        render.onload = function(e) {
            // @ts-ignore
            preview.setAttribute('src', e.target.result);
        };
        render.readAsDataURL(file);
        this.setState({file: e.target.files[0]})
    }


    init(){
        let username = getCookie("USERNAME")
        if (!username){
            window.location.href="/login";
        }
    }

    componentDidMount(){

    }

    handleMenuClick = (e) => {
        this.setState({channelName: e.domEvent.innerText})
        this.setState({channelId: e.key})
        console.log('click', e);
    }


    handleSubmit = (e) => {
        e.preventDefault()
        console.log(this.state.file)
        let {email, facebookLink, fullName, phone, positions, file} = this.state;
        if (!(validMail(email) !== undefined |
            validFullName(fullName) !== undefined |
            validPhone(phone) !== undefined |
            validFullName(positions) !== undefined |
            validProfileFacebook(facebookLink) !== undefined)){
            if (file === ''){
                this.setState({avatar: "avatar.png"})
                ENameCardService.createEmployee(this.state).then((res) => {
                    if (res.status === 400){
                        alert("sai định dạng dữ liệu!")
                    }
                    alert("create e-name card successfully")
                    this.props.history.push('/name-cards')
                }).catch(function (error) {
                    alert(error.response.data.error)
                });
            }else {
                ENameCardService.uploadAvatar(this.state.file).then((res) => {
                    this.setState({avatar: res.data.data})
                    ENameCardService.createEmployee(this.state).then((res) => {
                        alert("create e-name card successfully")
                        this.props.history.push('/name-cards')
                    },
                        error => {
                            console.log(error)
                        }
                    )
                })
            }
        }else {
            alert("Tồn tại trường dữ liệu không hợp lệ. Đề nghị kiểm tra lại!")
        }
    }

    handleChangeChannel = (value) => {
        // eslint-disable-next-line react/no-direct-mutation-state
        console.log(value)
        this.state.channelId = value.key;
    }


    render() {


        return(
            <div>
                <Form className="modal-content" onSubmit={this.handleSubmit}>
                    <div>
                        <div className="container">
                            <div className="avatar-div">
                                <img src={avatar} style={{"width":"200px" ,"height":"200px"}} id="viewAvatarP" />
                                <label>Chọn ảnh đại diện: </label>
                                <input type="file" onChange={this.uploadImage} />
                            </div>
                        </div>
                        <div className="content">
                            <div className="left">
                                <label htmlFor="fullName"><b>Mã đại lý:</b></label>
                                <Input type="text"
                                       value={this.state.codeAgent}
                                       onChange={this.handleChange}
                                       placeholder="Nhập mã đại lý" name="codeAgent"
                                />
                                <label htmlFor="fullName"><b>Họ tên:</b></label>
                                <Input type="text"
                                       value={this.state.fullName}
                                       onChange={this.handleChange}
                                       placeholder="Nhập họ tên" name="fullName"
                                       validations={[validFullName]}
                                />

                                <label htmlFor="phone"><b>Số điện thoại:</b></label>
                                <Input validations={[validPhone]}
                                       type="text"
                                       value={this.state.phone}
                                       onChange={this.handleChange}
                                       placeholder="Nhập số điện thoại" name="phone"/>
                                <label htmlFor="email"><b>Email:</b></label>
                                <Input validations={[validMail]}
                                       type="text"
                                       value={this.state.email}
                                       onChange={this.handleChange}
                                       placeholder="Nhập email" name="email"/>

                            </div>
                            <div className="right">
                                <div>
                                    <label htmlFor="facebookLink"><b>Kênh:</b></label>
                                    <Select
                                        labelInValue
                                        defaultValue={{ value: "Tên kênh" }}
                                        style={{ width: 500 }}
                                        onChange={this.handleChangeChannel}
                                        // disabled = {this.state.disableChannel}
                                    >
                                        {this.state.channels.map(channel => <Option value={channel.id}>{channel.name}</Option>)}
                                    </Select>
                                </div>
                                <label style={{"margin-top":"7.5%"}} htmlFor="facebookLink"><b>Facebook Link:</b></label>
                                <Input validations={[validProfileFacebook]}
                                       type="text"
                                       value={this.state.facebookLink}
                                       onChange={this.handleChange}
                                       placeholder="Nhập link facebook" name="facebookLink" />
                                <div>
                                    <label htmlFor="positions"><b>Chức vụ:</b></label>
                                    <Input validations={[validFullName]}
                                           type="text"
                                           value={this.state.positions}
                                           onChange={this.handleChange}
                                           placeholder="Nhập chức vụ" name="positions"/>
                                </div>
                            </div>
                        </div>
                        <div className="center">
                            <button type="submit" className="btn btn-success">Tạo</button>
                        </div>
                    </div>
                </Form>
            </div>
        )
    }
}

export default ENameCardCreate;
