package com.vsii.enamecard.services;

import com.vsii.enamecard.model.response.ChannelResponse;

import java.util.List;

public interface ChannelService {

    List<ChannelResponse> getList();
}
