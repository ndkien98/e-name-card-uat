package com.vsii.enamecard.services.impl;

import com.vsii.enamecard.model.response.ChannelResponse;
import com.vsii.enamecard.repositories.ChannelRepository;
import com.vsii.enamecard.services.ChannelService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ChannelServiceImpl implements ChannelService {


    private final ChannelRepository repository;
    private final ModelMapper modelMapper;

    public ChannelServiceImpl(ChannelRepository repository, ModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
    }

    public List<ChannelResponse> getList(){
        return repository.findAll().stream().map(channelEntity -> modelMapper.map(channelEntity,ChannelResponse.class)).collect(Collectors.toList());
    }
}
