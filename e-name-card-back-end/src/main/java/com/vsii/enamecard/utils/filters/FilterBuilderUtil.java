package com.vsii.enamecard.utils.filters;

import com.vsii.enamecard.exceptions.HttpErrorException;
import com.vsii.enamecard.utils.StringResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public final class FilterBuilderUtil {
    private static final Logger logger = LogManager.getLogger(FilterBuilderUtil.class);

    private FilterBuilderUtil() {

    }

    public static List<SearchCriteria> createFilterCondition(String criteria) {
        Pattern numberPattern = Pattern.compile("^([1-9]+[0-9]*\\.?[0-9]+|0\\.[0-9]+|[0-9])$");
        Pattern dateTimePattern = Pattern.compile("^(?:[1-9]\\d{3}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1\\d|2[0-8])|(?:0[13-9]|1[0-2])-(?:29|30)|(?:0[13578]|1[02])-31)|(?:[1-9]\\d(?:0[48]|[2468][048]|[13579][26])|(?:[2468][048]|[13579][26])00)-02-29)T(?:[01]\\d|2[0-3]):[0-5]\\d:[0-5]\\d(?:Z|[+-][01]\\d:[0-5]\\d)$");
        Pattern booleanPattern = Pattern.compile("(true)\\| (false)");
        List<SearchCriteria> filters = new ArrayList<>();
        if (!StringUtils.hasText(criteria)) return filters;

        final String FILTER_SEARCH_DELIMITER = "&";
        final String FILTER_CONDITION_DELIMITER = "\\|";
        final String LIST_DELIMITER = ",";

        List<String> expressions = splitStringToList(criteria, FILTER_SEARCH_DELIMITER);
        if (expressions.isEmpty()) return filters;
        expressions.forEach(x -> {
            logger.info(x);
            List<String> filter = splitStringToList(x, FILTER_CONDITION_DELIMITER);
            try {
                SearchOperator operator = SearchOperator.fromValue(filter.get(1));
                String stringValue = filter.get(2);
                String field = filter.get(0);
                if (operator != null) {
                    if (numberPattern.matcher(stringValue).matches()) {
                        filters.add(new SearchCriteria<Number>(field,
                                operator,
                                Long.parseLong(stringValue)));
                    } else if (dateTimePattern.matcher(stringValue).matches()) {
                        filters.add(new SearchCriteria<>(field,
                                operator,
                                ZonedDateTime.parse(stringValue).toOffsetDateTime()));
                    } else if (booleanPattern.matcher(stringValue).matches()) {
                        filters.add(
                                new SearchCriteria<>(field, operator,
                                        Boolean.valueOf(stringValue)));
                    } else if (operator == SearchOperator.IN) {
                        filters.add(
                                new SearchCriteria<>(field, operator,
                                        splitStringToListNumber(stringValue, LIST_DELIMITER)));
                    } else {
                        filters.add(
                                new SearchCriteria<>(field, operator,
                                        stringValue));
                    }
                }
            } catch (IndexOutOfBoundsException e) {
                throw HttpErrorException.badRequest(StringResponse.MISS_DATA_OF_FIELD);
            }
        });

        return filters;
    }

    private static List<String> splitStringToList(String search, String delimiter) {
        return Arrays.asList(search.split(delimiter));
    }

    private static List<Integer> splitStringToListNumber(String search, String delimiter) {
        return Arrays.stream(search.split(delimiter)).map(Integer::parseInt).collect(Collectors.toList());
    }

}
