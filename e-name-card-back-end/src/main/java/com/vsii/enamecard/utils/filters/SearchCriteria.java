package com.vsii.enamecard.utils.filters;

public class SearchCriteria<T> {
    private String key;
    private T value;
    private SearchOperator operation;

    public SearchCriteria() {
        // default constructor
    }

    public SearchCriteria(String key, SearchOperator operation, T value) {
        this.key = key;
        this.value = value;
        this.operation = operation;
    }

    // getters and setters, equals(), toString(), ... (omitted for brevity)

    public String getKey() {
        return key;
    }

    public SearchCriteria setKey(String key) {
        this.key = key;
        return this;
    }

    public T getValue() {
        return value;
    }

    public SearchCriteria setValue(T value) {
        this.value = value;
        return this;
    }

    public SearchOperator getOperation() {
        return operation;
    }

    public SearchCriteria setOperation(SearchOperator operation) {
        this.operation = operation;
        return this;
    }

    @Override
    public String toString() {
        return "SearchCriteria{" +
                "key='" + key + '\'' +
                ", value=" + value +
                ", operation=" + operation +
                '}';
    }
}
